﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torretz;

namespace DungeonGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            //You can define the size you want to the maze 
            //But I recommend from 10 to 250
            GameBoard GameBoard = MazeGenerator.getInstance().generateGameBoard(10);

            //This Prints to the output a maze and a gameBoard
            //To access any block of the gameboard you should use
            //GlobalVariables.getInstance().getBlock(x, y);
            GameBoard.printMaze();
            GameBoard.printGameBoard();
        }
    }
}
