﻿using System;

namespace Torretz{
	public enum RoomType{
		PILLAR,
		WALL,
		BREAKED_WALL,
		ROOM,
		PLAYER_SPAWNER,
		ENEMY_SPAWNER
	}
}

