﻿using System;

namespace Torretz{
	public class Room{
		private int x;
		private int y;
		private RoomType roomType;
		private int[,] blocks;

		public Room(int x, int y, RoomType type) {
			this.x = x;
			this.y = y;
			this.roomType = type;
		}

		public RoomType getType() {
			return roomType;
		}

		public void setRoomType(RoomType roomType) {
			this.roomType = roomType;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public int[,] getBlocks() {
			return blocks;
		}

		public void setBlocks(int[,] blocks) {
			this.blocks = blocks;
		}
	}
}

