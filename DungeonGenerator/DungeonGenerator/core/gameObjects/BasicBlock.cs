﻿using Torretz.core.Utilities;

namespace Torretz{
	public class BasicBlock : IBlock {
		protected BlockType blockType;
		protected Position position;
		 
		public BasicBlock(int x, int y, BlockType blockType){
			this.blockType = blockType;
            position = new Position (x,y);
		}

		public BlockType getBlockType() {
			return blockType;
		}

		public int getBoardPositionX(){
			return position.getX ();
		}

		public int getBoardPositionY(){
			return position.getY ();
		}
	}
}

