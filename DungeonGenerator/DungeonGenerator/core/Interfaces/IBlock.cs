﻿
namespace Torretz{
	public interface IBlock{

        BlockType getBlockType();

		int getBoardPositionX();

        int getBoardPositionY();

	}
}

