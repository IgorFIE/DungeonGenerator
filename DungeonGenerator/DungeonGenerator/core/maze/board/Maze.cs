﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Torretz{
	public class Maze{
		private Room[,] maze;
		private List<Room> rooms;
		private List<Room> walls;
		private List<Room> breakedWalls;
		private List<Room> enemySpawners;
		private Room playerSpawner;

		public Maze(int i){
			if(i % 2 == 0) i++;
			maze = new Room[i,i];
			rooms = new List<Room>();
			walls = new List<Room>();
			breakedWalls = new List<Room>();
			enemySpawners = new List<Room>();
			createTemplate();
		}

		private void createTemplate(){
			for(int x = 0; x < maze.GetLength(0); x++){
				for(int y = 0; y < maze.GetLength(0); y++){
					maze[x,y] = blockGeneratorBrain(x,y);
				}
			}
		}

		private Room blockGeneratorBrain(int x, int y){
			Room room;
			if ((room = isWall(x,y)) != null) return room;
			if ((room = isRoom(x,y)) != null) return room;
			return new Room(x,y, RoomType.PILLAR);
		}

		private Room isWall(int x, int y) {
			if(x != 0 && x != maze.GetLength(0) - 1 && y != 0 && y != maze.GetLength(0) - 1 ) {
				if (x % 2 == 1 && y % 2 == 0 || x % 2 == 0 && y % 2 == 1){
					Room room = new Room(x, y, RoomType.WALL);
					walls.Insert(walls.Count,room);
					return room;
				}
			}
			return null;
		}

		private Room isRoom(int x, int y) {
			if (x != 0 && x != maze.GetLength(0) - 1 && x % 2 == 1 && y != 0 && y != maze.GetLength(0) - 1 && y % 2 == 1) {
				Room room = new Room(x,y, RoomType.ROOM);
				rooms.Insert(rooms.Count,room);
				return room;
			}
			return null;
		}

		//GETTERS

		public int getBoardSize() {
			return maze.GetLength(0);
		}

		public Room getBlock(int x, int y) {
			return maze[x,y];
		}

		public List<Room> getRooms(){
			return rooms;
		}

		public List<Room> getWalls() {
			return walls;
		}

		public List<Room> getBreakedWalls() {
			return breakedWalls;
		}

		public void setPlayerSpawner(Room playerSpawners) {
			this.playerSpawner = playerSpawners;
		}

		public void setEnemySpawner(Room enemySpawners) {
			this.enemySpawners.Insert(this.enemySpawners.Count,enemySpawners);
		}

		public List<Room> getEnemySpawners() {
			return enemySpawners;
		}

		public Room getPlayerSpawner() {
			return playerSpawner;
		}

		//PRINT MAZE ON CONSOLE
		public void printMaze(){
			String line;
			for(int x = maze.GetLength(0) - 1; x >= 0; x--){
				line = "";
				for(int y = 0; y < maze.GetLength(0); y++){
					if((maze[x,y]).getType() == RoomType.PILLAR) line += "# ";
					if((maze[x,y]).getType() == RoomType.WALL) line += "w ";
					if((maze[x,y]).getType() == RoomType.BREAKED_WALL) line += "  ";
					if((maze[x,y]).getType() == RoomType.ROOM) line += "  ";
					if((maze[x,y]).getType() == RoomType.PLAYER_SPAWNER) line += "P ";
					if((maze[x,y]).getType() == RoomType.ENEMY_SPAWNER) line += "E ";
				}
				Debug.WriteLine(line);
			}
		}
	}
}

