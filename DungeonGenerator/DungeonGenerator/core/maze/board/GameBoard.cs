﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Torretz.core.Utilities;

namespace Torretz{
	public class GameBoard{
		private Maze maze;

		public GameBoard(Maze maze) {
			this.maze = maze;
			convertMazeToGameBoard(maze);
		}

		private void convertMazeToGameBoard(Maze maze){
			int blockSize = (maze.getBoardSize () / 2) * GameProperties.ROOM_HEIGHT_SIZE;
			GlobalVariables.getInstance().setBlocks (new IBlock[blockSize,blockSize]);

			LinkedList<Room> roomList;
			int height = 0;
			int width;

			for(int x = maze.getBoardSize() - 1; x >= 0; x--){
				roomList = new LinkedList<Room> ();
				for(int y = 0; y < maze.getBoardSize(); y++){
					Room room = maze.getBlock (x, y);
					RoomType roomType = room.getType ();
					if(roomType == RoomType.ROOM || roomType == RoomType.PLAYER_SPAWNER || roomType == RoomType.ENEMY_SPAWNER){
						roomList.AddLast(room);
					}
				}
				if(roomList.Count != 0) {
					int count = GameProperties.ROOM_HEIGHT_SIZE -1;
					while (count >= 0) {
						width = 0;
						foreach (Room room in roomList) {
							for(int xx = 0; xx < room.getBlocks().GetLength(0); xx++){
								if (room.getBlocks()[xx,count] == 0) {
									GlobalVariables.getInstance().setBlock(new BasicBlock(width,height, BlockType.FLOOR),width,height);
								}else if (room.getBlocks()[xx,count] == 1){
									GlobalVariables.getInstance().setBlock(new BasicBlock(width,height, BlockType.ENEMY_SPAWNER),width,height);
								}else if (room.getBlocks()[xx,count] == 2){
									GlobalVariables.getInstance().setBlock(new BasicBlock(width,height, BlockType.PLAYER),width,height);
								} else {
									GlobalVariables.getInstance().setBlock(new BasicBlock(width,height, BlockType.WALL),width,height);
								}
								width++;
							}
						}
						height++;
						count--;
					}
				}
			}
		}

		public void printMaze(){
			maze.printMaze();
		}

		public void printGameBoard(){
			String line;
			for(int x = 0; x < GlobalVariables.getInstance().gameBoardLenght (); x++){
				line = "";
				for(int y = 0; y < GlobalVariables.getInstance().gameBoardLenght (); y++){
					if(GlobalVariables.getInstance().getBlock(y,x).getBlockType() == BlockType.FLOOR) line += "  ";
					if(GlobalVariables.getInstance().getBlock(y,x).getBlockType() == BlockType.WALL) line += "# ";
					if(GlobalVariables.getInstance().getBlock(y,x).getBlockType() == BlockType.ENEMY_SPAWNER) line += "E ";
					if(GlobalVariables.getInstance().getBlock(y,x).getBlockType() == BlockType.PLAYER) line += "P ";
				}
                Debug.WriteLine(line);
			}
		}
	}
}