﻿using System;

namespace Torretz
{
	public class SpawnBuilder
	{
		private Maze maze;
		private Random random;

		public SpawnBuilder(Maze maze) {
			this.maze = maze;
			random = new Random ();
			generateSpawners();
		}

		private void generateSpawners(){
			generatePlayerSpawner();
			generateEnemySpawners();
		}

		private void generatePlayerSpawner(){
			int randomPlayerRoom = random.Next(maze.getRooms().Count);
			Room room = maze.getRooms ()[randomPlayerRoom];
			room.setRoomType(RoomType.PLAYER_SPAWNER);
			maze.setPlayerSpawner(room);

			maze.getRooms().RemoveAt(randomPlayerRoom);
		}

		private void generateEnemySpawners(){
			int numberOfSpawners = maze.getRooms().Count/8;
			int randomEnemyRoom;
			int count = 0;
			Room room;
			while(count < numberOfSpawners){
				randomEnemyRoom = random.Next(maze.getRooms().Count);
				room = maze.getRooms()[randomEnemyRoom];
				if(checkRangeToPlayerSpawner(room)){
					room.setRoomType(RoomType.ENEMY_SPAWNER);
					maze.setEnemySpawner(room);
					maze.getRooms().RemoveAt(randomEnemyRoom);
					count++;
				}
			}
		}

		private bool checkRangeToPlayerSpawner(Room roomToCheck){
			int range = 3;

			if(maze.getPlayerSpawner().getX() + range <= roomToCheck.getX()){
				if(maze.getPlayerSpawner().getY() <= roomToCheck.getY() 
					|| maze.getPlayerSpawner().getY() >= roomToCheck.getY()){
					return true;
				}
			}
			if(maze.getPlayerSpawner().getX() - range > roomToCheck.getX()){
				if(maze.getPlayerSpawner().getY() <= roomToCheck.getY() 
					|| maze.getPlayerSpawner().getY() >= roomToCheck.getY()){
					return true;
				}
			}
			if(maze.getPlayerSpawner().getY() + range < roomToCheck.getY()){
				if(maze.getPlayerSpawner().getX() <= roomToCheck.getX() 
					|| maze.getPlayerSpawner().getX() >= roomToCheck.getX()){
					return true;
				}
			}
			if(maze.getPlayerSpawner().getY() - range > roomToCheck.getY()){
				if(maze.getPlayerSpawner().getX() <= roomToCheck.getX() 
					|| maze.getPlayerSpawner().getX() >= roomToCheck.getX()){
					return true;
				}
			}
			return false;
		}
	}
}

