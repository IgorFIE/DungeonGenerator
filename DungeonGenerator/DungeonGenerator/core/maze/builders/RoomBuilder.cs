﻿using System;
using Torretz.core.Utilities;

namespace Torretz
{
	public class RoomBuilder
	{
		private Maze maze;
		private Random random;

		public RoomBuilder(Maze maze) {
			this.maze = maze;
			random = new Random ();
			generateBlocks();
		}

		public void generateBlocks(){
			foreach (Room room in maze.getRooms()) {
				room.setBlocks(createBlocks(room));
			}
			foreach (Room room in maze.getEnemySpawners()) {
				room.setBlocks(createEnemySpawnerBlocks(room));
			}
			maze.getPlayerSpawner().setBlocks(createPlayerSpawnerBlocks(maze.getPlayerSpawner()));
		}

		private int[,] createPlayerSpawnerBlocks(Room room){
			int[,] createdBlocks = createBlocks(room);
			createdBlocks[GameProperties.ROOM_HEIGHT_SIZE /2,(GameProperties.ROOM_WITH_SIZE /2)-1] = 2;
			return createdBlocks;
		}

		private int[,] createEnemySpawnerBlocks(Room room){
			int[,] createdBlocks = createBlocks(room);
			createdBlocks[GameProperties.ROOM_HEIGHT_SIZE /2,(GameProperties.ROOM_WITH_SIZE /2)-1] = 1;
			return createdBlocks;
		}

		private int[,] createBlocks(Room room) {
			int[,] blocks = new int[GameProperties.ROOM_HEIGHT_SIZE, GameProperties.ROOM_WITH_SIZE];

			int width = random.Next(GameProperties.ROOM_WITH_SIZE - GameProperties.HALL_SIZE) + GameProperties.ROOM_MIN_SIZE;
			int height = random.Next(GameProperties.ROOM_HEIGHT_SIZE - GameProperties.HALL_SIZE) + GameProperties.ROOM_MIN_SIZE;

			int widthDifference = (blocks.GetLength(0) - width)/2;
			int heightDifference = (blocks.GetLength(0) - height)/2;


			for(int x = blocks.GetLength(0) - 1; x >= 0; x--){
				for(int y = 0; y < blocks.GetLength(0); y++){
					if(x == 0 || x == blocks.GetLength(0)-1 || y == 0 || y == blocks.GetLength(0)-1){
						blocks[x,y] = 3;
						continue;
					}
					if(y < widthDifference-1){
						blocks[x,y] = 3;
						continue;
					}

					if(y > blocks.GetLength(0) - widthDifference){
						blocks[x,y] = 3;
						continue;
					}

					if(x < heightDifference-1){
						blocks[x,y] = 3;
						continue;
					}

					if(x > blocks.GetLength(0) - heightDifference){
						blocks[x,y] = 3;
						continue;
					}
					blocks[x,y] = 0;
				}
			}
			return breakWalls(blocks,room);
		}

		private int[,] breakWalls(int[,] blocks, Room room) {
			if(maze.getBreakedWalls().Contains(maze.getBlock(room.getX()+1,room.getY()))){
				for(int x = blocks.GetLength(0) - 1; x >= 0; x--){
					for(int y = 0; y < blocks.GetLength(0); y++){
						if(x > (GameProperties.ROOM_HEIGHT_SIZE /2) - 2 
							&& x < GameProperties.ROOM_HEIGHT_SIZE - (GameProperties.ROOM_HEIGHT_SIZE /2) + 1 
							&& y > (GameProperties.ROOM_WITH_SIZE /2)){
							blocks[x,y] = 0;
						}
					}
				}
			}
			if(maze.getBreakedWalls().Contains(maze.getBlock(room.getX()-1,room.getY()))){
				for(int x = blocks.GetLength(0) - 1; x >= 0; x--){
					for(int y = 0; y < blocks.GetLength(0); y++){
						if(x > (GameProperties.ROOM_HEIGHT_SIZE /2) - 2 
							&& x < GameProperties.ROOM_HEIGHT_SIZE - (GameProperties.ROOM_HEIGHT_SIZE /2) + 1 
							&& y < (GameProperties.ROOM_WITH_SIZE /2)){
							blocks[x,y] = 0;
						}
					}
				}
			}
			if(maze.getBreakedWalls().Contains(maze.getBlock(room.getX(),room.getY()+1))){
				for(int x = blocks.GetLength(0) - 1; x >= 0; x--){
					for(int y = 0; y < blocks.GetLength(0); y++){
						if(y > (GameProperties.ROOM_WITH_SIZE /2) - 3 
							&& y < GameProperties.ROOM_WITH_SIZE - (GameProperties.ROOM_WITH_SIZE /2) + 1 
							&& x > (GameProperties.ROOM_HEIGHT_SIZE /2)){
							blocks[x,y] = 0;
						}
					}
				}
			}
			if(maze.getBreakedWalls().Contains(maze.getBlock(room.getX(),room.getY()-1))){
				for(int x = blocks.GetLength(0) - 1; x >= 0; x--){
					for(int y = 0; y < blocks.GetLength(0); y++){
						if(y > (GameProperties.ROOM_WITH_SIZE /2) - 3 
							&& y < GameProperties.ROOM_WITH_SIZE - (GameProperties.ROOM_WITH_SIZE /2) + 1 
							&& x < (GameProperties.ROOM_HEIGHT_SIZE /2)){
							blocks[x,y] = 0;
						}
					}
				}
			}
			//print(blocks);
			return blocks;
		}

		private void print(int[,] blocks) {
			String line;
			for(int y = blocks.GetLength(0) - 1; y >= 0; y--) {
				line = "";
				for (int x = 0; x < blocks.GetLength(0); x++) {
					if(blocks[x,y] == 0){
						line += "  ";
					} else {
						line += "# ";
					}
				}
				Console.WriteLine(line);
			}
			Console.WriteLine("___________________________________________________________");
		}
	}
}

