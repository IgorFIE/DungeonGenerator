﻿using System;
using System.Collections.Generic;

namespace Torretz
{
	public class MazeBuilder
	{
		protected Maze maze;
		protected HashSet<Room> visitedRooms;
		protected List<Room> path;
		private Random random; 

		public MazeBuilder(Maze maze) {
			this.maze = maze;
			visitedRooms = new HashSet<Room>();
			path = new List<Room>();
			random = new Random ();
			createMaze();
			breakExtraWalls();
		}

		protected void createMaze(){
			int randomBlock = random.Next(maze.getRooms().Count);
			Room currentRoom = maze.getRooms()[randomBlock];

			visitedRooms.Add(currentRoom);
			path.Insert(0,currentRoom);
			int way;

			while(visitedRooms.Count < maze.getRooms().Count){
				way = random.Next(4);
				currentRoom = moveOptions(currentRoom, way);
			}
		}

		protected Room moveOptions(Room currentRoom, int way) {
			if(way == 0) {
				return moveUp(currentRoom);
			}
			else if(way == 1){
				return moveDown(currentRoom);
			}
			else if(way == 2){
				return moveRight(currentRoom);
			}
			else {
				return moveLeft(currentRoom);
			}
		}

		protected Room moveUp(Room currentRoom){
			if(currentRoom.getY() < maze.getBoardSize()-2){
				if(hasBeenVisited(currentRoom,0,2)){
					return preformMovement(currentRoom.getX(), currentRoom.getY()+2);
				}
			}
			return checkForPossibleMovements(currentRoom);
		}

		protected Room moveDown(Room currentRoom){
			if(currentRoom.getY() > 1){
				if (hasBeenVisited(currentRoom,0,-2)){
					return preformMovement(currentRoom.getX(), currentRoom.getY()-2);
				}
			}
			return checkForPossibleMovements(currentRoom);
		}

		protected Room moveRight(Room currentRoom){
			if(currentRoom.getX() < maze.getBoardSize()-2){
				if (hasBeenVisited(currentRoom,2,0)){
					return preformMovement(currentRoom.getX() + 2, currentRoom.getY());
				}
			}
			return checkForPossibleMovements(currentRoom);
		}

		protected Room moveLeft(Room currentRoom){
			if(currentRoom.getX() > 1){
				if (hasBeenVisited(currentRoom,-2,0)){
					return preformMovement(currentRoom.getX() - 2, currentRoom.getY());
				}
			}
			return checkForPossibleMovements(currentRoom);
		}

		private bool hasBeenVisited(Room currentRoom, int x, int y) {
			if(!visitedRooms.Contains(maze.getBlock(currentRoom.getX()+(x), currentRoom.getY()+(y)))){
				if(x == -2) x = -1;if(x == 2) x = 1;if(y == -2) y = -1;if(y == 2) y = 1;
				Room wallToBreak = maze.getBlock(currentRoom.getX()+(x), currentRoom.getY()+(y));
				wallToBreak.setRoomType(RoomType.BREAKED_WALL);
				maze.getBreakedWalls().Add(wallToBreak);
				maze.getWalls().Remove(wallToBreak);
				return true;
			}
			return false;
		}

		private Room preformMovement(int x, int y) {
			Room currentRoom = maze.getBlock(x, y);
			visitedRooms.Add(currentRoom);
			path.Insert(0,currentRoom);
			return currentRoom;
		}

		private Room checkForPossibleMovements(Room currentRoom) {
			List<Int16> possibleDirections;
			if((possibleDirections = hasPossibleDirections(currentRoom)).Count != 0){
				int way = possibleDirections[random.Next(possibleDirections.Count-1)];
				return moveOptions(currentRoom,way);
			}
			path.RemoveAt(0);
			return path[0];
		}

		private List<Int16> hasPossibleDirections(Room currentRoom) {
			List<Int16> possibleDirections = new List<Int16> ();
			if(currentRoom.getY() < maze.getBoardSize()-2){
				if(!visitedRooms.Contains(maze.getBlock(currentRoom.getX(), currentRoom.getY()+2))) {
					possibleDirections.Add(0);
				}
			}
			if(currentRoom.getY() > 1){
				if(!visitedRooms.Contains(maze.getBlock(currentRoom.getX(), currentRoom.getY()-2))){
					possibleDirections.Add(1);
				}
			}
			if(currentRoom.getX() < maze.getBoardSize()-2) {
				if (!visitedRooms.Contains(maze.getBlock(currentRoom.getX() + 2, currentRoom.getY()))) {
					possibleDirections.Add(2);
				}
			}
			if(currentRoom.getX() > 1) {
				if (!visitedRooms.Contains(maze.getBlock(currentRoom.getX() - 2, currentRoom.getY()))) {
					possibleDirections.Add(3);
				}
			}
			return possibleDirections;
		}

		private void breakExtraWalls(){
			int positionToBreak;
			int numberOfWallsToBreak = maze.getWalls().Count/2 + maze.getWalls().Count/4;
			while(maze.getWalls().Count>numberOfWallsToBreak){
				positionToBreak = random.Next(maze.getWalls().Count);
				Room room = maze.getWalls()[positionToBreak];
				room.setRoomType(RoomType.BREAKED_WALL);
				maze.getBreakedWalls().Insert(maze.getBreakedWalls().Count,room);
				maze.getWalls().RemoveAt(positionToBreak);
			}
		}
	}
}

