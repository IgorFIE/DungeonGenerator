﻿using System;

namespace Torretz{
	public class MazeGenerator{

		private Maze maze;
		private MazeBuilder mazeBuilder;
		private SpawnBuilder spawnBuilder;
		private RoomBuilder roomBuilder;
		private GameBoard gameBoard;

		private static MazeGenerator globalGenerators;

		private MazeGenerator(){}

		public static MazeGenerator getInstance(){
			if(globalGenerators == null){
				globalGenerators = new MazeGenerator();
			}
			return globalGenerators;
		}

		public GameBoard generateGameBoard(int size){
			maze = new Maze(size);
			mazeBuilder = new MazeBuilder(maze);
			spawnBuilder = new SpawnBuilder(maze);
			roomBuilder = new RoomBuilder(maze);
			gameBoard = new GameBoard(maze);
			GC.Collect();
			GC.WaitForPendingFinalizers();
			return gameBoard;
		}


	}
}

