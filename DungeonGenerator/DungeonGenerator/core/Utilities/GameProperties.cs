﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torretz.core.Utilities
{
    class GameProperties
    {
        //Costumaze your Rooms here
        public const int ROOM_WITH_SIZE = 24;
        public const int ROOM_HEIGHT_SIZE = 23;
        public const int ROOM_MIN_SIZE = 6;
        public const int HALL_SIZE = 3;
    }
}
