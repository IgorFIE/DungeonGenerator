﻿
namespace Torretz
{
    public class GlobalVariables{

		private static GlobalVariables globalVariables;

		private IBlock[,] blocks;

		private GlobalVariables(){}

		public static GlobalVariables getInstance(){
			if (globalVariables == null) {
				globalVariables = new GlobalVariables ();
			}
			return globalVariables;
		}

		public int gameBoardLenght(){
			return blocks.GetLength (0);
		}

		public void setBlocks(IBlock[,] blocks){
			this.blocks = blocks;
		}

		public IBlock getBlock(int x, int y){
			return blocks [x, y];
		}

		public void setBlock(IBlock block, int x, int y){
			blocks [x, y] = block;
		}
	}
}

